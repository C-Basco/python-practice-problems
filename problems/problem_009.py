# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    rev_word = word[::-1]
    if word == rev_word:
        print("It's a palindrome!!!")
    else:
        print("It is not a palindrome")


print("Let's find out a word is a palindrome!")
print("Enter a word: ")
word = input()

is_palindrome(word)
