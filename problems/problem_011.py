# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_5(number):
    if number % 5 == 0:
        print("Buzz")
    else:
        print("Not divisble by 5")

print("Let's find out if this number is divisble by 5")
print("Enter a number: ")
number = input()
int_number = int(number)

is_divisible_by_5(int_number)
