# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18:
        print("You are eligble to SkyDive")
    elif has_consent_form == "yes":
        print("You have a consent form you may Sky Dive")
    elif has_consent_form == "no":
        print("You are not eligble to Sky Dive")
    else:
        print("There are issues with your response please retry")

print("Are you eligble to Sky Dive. Find out!")
print("How old are you?: ")
age = input()
age = int(age)
print("Do you have a signed consent form? yes/no ?: ")
consent_form = input()
consent_form = consent_form.lower()

can_skydive(age,consent_form)
